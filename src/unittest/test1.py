#!/usr/bin/env python3
#
# Copyright (C) Anton Liaukevich 2011-2015 <leva.dev@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import unittest
from limitedapt.packages import VersionedPackage
from limitedapt.enclosure import Enclosure


class Test(unittest.TestCase):

    def test1(self):
        enclosure1 = Enclosure()
        enclosure1.import_from_xml("data/enclosure1")
        self.assertTrue(VersionedPackage("3dchess", "kfreebsd-amd64", "0.8.1-17") in enclosure1)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()