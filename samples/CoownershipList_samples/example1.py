#!/usr/bin/env python3


'''
Created on 23 дек. 2014 г.

@author: anthony
'''


from limitedapt.packages import ConcretePackage
from limitedapt.coownership import CoownershipList
from limitedapt.coownership import XmlImportSyntaxError


def create_list_1():
    mylist = CoownershipList()
    
    pkg4 = ConcretePackage("3dchess", "amd64")
    mylist.add_ownership(pkg4, "root2")
    
    pkg1 = ConcretePackage("extremetuxracer", "amd64")
    mylist.add_ownership(pkg1, "anthony")
    mylist.add_ownership(pkg1, "galina")
    mylist.add_ownership(pkg1, "henady")
    mylist.add_ownership(pkg1, "a")
    mylist.add_ownership(pkg1, "g")
    mylist.add_ownership(pkg1, "hey")
    mylist.add_ownership(pkg1, "4")
    mylist.add_ownership(pkg1, "1")
    mylist.add_ownership(pkg1, "2")

    pkg2 = ConcretePackage("extremetuxracer", "i386")
    mylist.add_ownership(pkg2, "olduser1")
    mylist.add_ownership(pkg2, "olduser2")
    
    pkg3 = ConcretePackage("3dchess", "amd64")
    mylist.add_ownership(pkg3, "root")
    
    return mylist

def main():
    list1 = create_list_1()
    list1.export_to_xml("/home/anthony/projects/liblimited-apt/samples/CoownershipList_samples/output1")
    #print(list1.export_to_xml())

    list2 = CoownershipList()
    try:
        list2.import_from_xml("/home/anthony/projects/liblimited-apt/samples/CoownershipList_samples/input1")
        list2.export_to_xml("/home/anthony/projects/liblimited-apt/samples/CoownershipList_samples/output2")
    except XmlImportSyntaxError as err:
        print(err)
        

if __name__ == '__main__':
    main()