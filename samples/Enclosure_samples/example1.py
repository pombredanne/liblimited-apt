#!/usr/bin/env python3

from limitedapt.enclosure import Enclosure


def main():
    enclosure1 = Enclosure()
    enclosure1.import_from_xml("enclosure1")
    enclosure1.export_to_xml("enclosure2")

if __name__ == '__main__':
    main()